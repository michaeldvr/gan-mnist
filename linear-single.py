import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import numpy as np
import scipy.misc
import os
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import time
import pickle

# hyperparam & config
max_iter = 100000
batch_size = 128
z_size = 100
learning_rate = 0.001
d_hidden = 128
g_hidden = 128
sample_interval = 1000
n_sample = 16
print_interval = 1000
save_dir = './output/linear/single'

def plot(samples):
    fig = plt.figure(figsize=(4, 4))
    gs = gridspec.GridSpec(4, 4)
    gs.update(wspace=0.05, hspace=0.05)

    for i, sample in enumerate(samples):
        ax = plt.subplot(gs[i])
        plt.axis('off')
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        ax.set_aspect('equal')
        plt.imshow(sample.reshape(28, 28), cmap='Greys_r')

    return fig

dataset = input_data.read_data_sets('MNIST_data/')


def save_images(path, images, batch_sz, n_sample):
    size = int(np.ceil(np.sqrt(batch_sz)))
    size = (size, size)
    images = images.reshape([n_sample, 28, 28, 1])
    imgs = (images + 1.) / 2.
    # imgs = images
    h, w = imgs.shape[1], imgs.shape[2]
    # join gambar
    img = np.zeros((int(h * size[0]), int(w * size[1]), 1))
    for idx, image in enumerate(imgs):
        i = idx % size[1]
        j = idx // size[1]
        img[j * h:j * h + h, i * w:i * w + w, :] = image
    img = np.reshape(img, (int(h * size[0]), int(w * size[1])))
    scipy.misc.imsave(path, (255 * img).astype(np.uint8))

if not os.path.exists(save_dir):
    os.mkdir(save_dir)

# build model
input_real = tf.placeholder(tf.float32, [None, 784], name='input_real')
input_z = tf.placeholder(tf.float32, [None, z_size], name='z')

var_g = []
with tf.variable_scope('generator'):
    '''ref: http://andyljones.tumblr.com/post/110998971763/an-explanation-of-xavier-initialization'''
    g_w1 = tf.get_variable('w1', [z_size, g_hidden],
                           initializer=tf.random_normal_initializer(stddev=tf.sqrt(2. / z_size)))
    g_b1 = tf.get_variable('b1', [g_hidden],
                           initializer=tf.zeros_initializer())
    g_w2 = tf.get_variable('w2', [g_hidden, 784],
                           initializer=tf.random_normal_initializer(stddev=tf.sqrt(2. / g_hidden)))
    g_b2 = tf.get_variable('b2', [784],
                           initializer=tf.zeros_initializer())
    var_g += [g_w1, g_b1, g_w2, g_b2]
    g_h1 = tf.nn.relu(tf.matmul(input_z, g_w1) + g_b1)
    g_logit = tf.matmul(g_h1, g_w2) + g_b2
    g = tf.nn.sigmoid(g_logit)

var_d = []
# real discriminator
with tf.variable_scope('discriminator'):
    d_w1 = tf.get_variable('w1', [784, d_hidden],
                           initializer=tf.random_normal_initializer(stddev=tf.sqrt(2. / 784)))
    d_b1 = tf.get_variable('b1', [d_hidden],
                           initializer=tf.zeros_initializer())
    d_w2 = tf.get_variable('w2', [d_hidden, 1],
                           initializer=tf.random_normal_initializer(stddev=tf.sqrt(2. / d_hidden)))
    d_b2 = tf.get_variable('b2', [1],
                           initializer=tf.zeros_initializer())
    var_d += [d_w1, d_b1, d_w2, d_b2]
    d_h1 = tf.nn.relu(tf.matmul(input_real, d_w1) + d_b1)
    d_logit = tf.matmul(d_h1, d_w2) + d_b2
    d = tf.nn.sigmoid(d_logit)

# fake discriminator
with tf.variable_scope('discriminator', reuse=True):
    d_w1 = tf.get_variable('w1', [784, d_hidden],
                           initializer=tf.random_normal_initializer(stddev=tf.sqrt(2. / 784)))
    d_b1 = tf.get_variable('b1', [d_hidden],
                           initializer=tf.zeros_initializer())
    d_w2 = tf.get_variable('w2', [d_hidden, 1],
                           initializer=tf.random_normal_initializer(stddev=tf.sqrt(2. / d_hidden)))
    d_b2 = tf.get_variable('b2', [1],
                           initializer=tf.zeros_initializer())
    d_h1 = tf.nn.relu(tf.matmul(g, d_w1) + d_b1)
    d_logit_ = tf.matmul(d_h1, d_w2) + d_b2
    d_ = tf.nn.sigmoid(d_logit_)

# losses
d_loss_real = tf.reduce_mean(
    tf.nn.sigmoid_cross_entropy_with_logits(logits=d_logit, labels=tf.ones_like(d_logit)))
d_loss_fake = tf.reduce_mean(
    tf.nn.sigmoid_cross_entropy_with_logits(logits=d_logit_, labels=tf.zeros_like(d_logit_)))
d_loss = d_loss_real + d_loss_fake
g_loss = tf.reduce_mean(
    tf.nn.sigmoid_cross_entropy_with_logits(logits=d_logit_, labels=tf.ones_like(d_logit_)))

d_opt = tf.train.AdamOptimizer().minimize(d_loss, var_list=var_d)
g_opt = tf.train.AdamOptimizer().minimize(g_loss, var_list=var_g)

sess = tf.Session()
sess.run(tf.global_variables_initializer())

losses = []

start_time = time.time()
print()
print(time.strftime('=== [ start: %d %b %Y %H:%M:%S ] ===', time.localtime()))

for i in range(max_iter + 1):

    if i % sample_interval == 0:
        sample_z = np.random.uniform(-1., 1., [n_sample, z_size])
        samples = sess.run(g, feed_dict={input_z: sample_z})
        # save_images(os.path.join(save_dir, '{}.png'.format(str(i).zfill(5))), samples, batch_size, n_sample)
        fig = plot(samples)
        plt.savefig(os.path.join(save_dir, '{}.png'.format(str(i).zfill(5))), bbox_inches='tight')
        plt.close(fig)

    sample_z = np.random.uniform(-1., 1., [batch_size, z_size])
    sample_imgs, _ = dataset.train.next_batch(batch_size)

    # train d
    _, d_loss_value = sess.run([d_opt, d_loss], feed_dict={input_real: sample_imgs, input_z: sample_z})
    # train g
    _, g_loss_value = sess.run([g_opt, g_loss], feed_dict={input_z: sample_z})

    losses.append((d_loss_value, g_loss_value))

    if i % print_interval == 0:
        print('Iter {:5d} | d_loss: {:.8f}, g_loss: {:.8f}'.format(i, d_loss_value, g_loss_value))

finish_time = time.time() - start_time
print('Training finished in {:2d}h {:2d}m {:7.4f}s'.
      format(int(finish_time // 3600), int((finish_time % 3600) // 60), finish_time % 60))

with open(os.path.join(save_dir, 'losses.pkl'), 'wb') as f:
    pickle.dump(losses, f)

fix, ax = plt.subplots()
losses = np.array(losses)
plt.plot(losses.T[0], label='Discriminator')
plt.plot(losses.T[1], label='Generator')
plt.title("Training Losses")
plt.legend()
plt.show()
