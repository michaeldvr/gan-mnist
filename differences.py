""" hitung absolute error minimum tiap kelas digit """
from helper.dataloader import MNIST
import pickle
from helper.utils import printProgressBar
import time
import numpy as np


OUTFILE = './output/diff_complete.txt'
DATA = './output/cgan/cgan_test/output_dict'
N_CLASS = 10


def mean_absolute_error(data1, data2):
    count = 784
    ctr = 0
    for i in range(count):
        diff = data1[i] - data2[i]
        if diff < 0:
            diff *= -1
        ctr += diff
    return ctr / count


def diff(imgs, labels, save_to=None, mnist=None, to_string=True):
    if mnist is None:
        mnist = MNIST(flatten=True, one_hot=False)
    n_data = len(imgs)
    data = [[] for i in range(N_CLASS)]
    ctr = [0 for i in range(N_CLASS)]

    sample_ctr = [0 for i in range(N_CLASS)]

    total_iter = n_data * mnist.num_examples - 1

    start_time = time.time()
    print()
    print(time.strftime('=== [ start: %d %b %Y %H:%M:%S ] ===', time.localtime()))
    try:

        for i in range(mnist.num_examples):
            target = mnist.labels[i]
            for j in range(n_data):
                printProgressBar((i * n_data) + j, total_iter, length=50, fill='#',
                                 suffix=str(i+1) + ' dari ' + str(mnist.num_examples) + ' digit mnist',
                                 prefix='{:3d} / {:3d}'.format(j+1, n_data))
                if labels[j] == target:
                    ctr[target] += 1
                    tmp = mean_absolute_error(mnist.images[i], imgs[j])
                    data[target].append(tmp)

    except KeyboardInterrupt:
        print('\nstopped')

    min_d = [min(data[i]) if ctr[i] > 0 else 'x' for i in range(N_CLASS)]
    max_d = [max(data[i]) if ctr[i] > 0 else 'x' for i in range(N_CLASS)]
    sum_d = [sum(data[i]) if ctr[i] > 0 else 'x' for i in range(N_CLASS)]
    avg_d = [(sum_d[i] / ctr[i]) if ctr[i] > 0 else 'x' for i in range(N_CLASS)]
    std_d = [np.std(data[i]) if ctr[i] > 0 else 'x' for i in range(N_CLASS)]

    for i in range(n_data):
        sample_ctr[labels[i]] += 1

    finish_time = time.time() - start_time
    duration = '{:2d}h {:2d}m {:7.4f}s'.\
        format(int(finish_time // 3600), int((finish_time % 3600) // 60), finish_time % 60)
    print('\nCalculation finished in', duration)
    if to_string:
        result = ''
        result += 'class\tcount\tmin\tmax\taverage\ttotal\tstd'
        for i in range(N_CLASS):
            result += '\n' + '\t'.join([
                str(i), str(ctr[i]), str(min_d[i]), str(max_d[i]), str(avg_d[i]),
                str(sum_d[i]), str(std_d[i])])
        result += '\n\nnum samples ' + str(n_data) + '\nclass\tcount'
        for i in range(N_CLASS):
            result += '\n' + str(i) + '\t' + str(sample_ctr[i])
        result += '\n\n' + duration

        if save_to is not None:
            with open(save_to, 'w') as f:
                f.write(result)
    else:
        res = [['class', 'count', 'min', 'max', 'average', 'total', 'std']]
        for i in range(N_CLASS):
            res.append([str(i), str(ctr[i]), str(min_d[i]), str(max_d[i]), str(avg_d[i]), str(sum_d[i]), str(std_d[i])])
        res2 = [['class', 'count']]
        for i in range(N_CLASS):
            res2.append([str(i), str(sample_ctr[i])])
        result = (res, res2)
    return result


if __name__ == '__main__':
    with open(DATA, 'rb') as f:
        samples = pickle.load(f)

    imgs = samples['images'].reshape((-1, 784))
    labels = samples['labels_single']