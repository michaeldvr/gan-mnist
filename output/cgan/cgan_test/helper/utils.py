﻿import numpy as np
import scipy.misc
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec


def save_images(path, images, imgsize, boxes=(4, 4)):
    fig = plot(images, boxes, imgsize)
    plt.savefig(path, bbox_inches='tight')
    plt.close(fig)


def plot(samples, boxes, imgsize):
    fig = plt.figure(figsize=boxes)
    gs = gridspec.GridSpec(boxes[0], boxes[1])
    gs.update(wspace=0.05, hspace=0.05)
    # print(samples.shape)
    for i, sample in enumerate(samples):
        ax = plt.subplot(gs[i])
        plt.axis('off')
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        ax.set_aspect('equal')
        tmp = sample.reshape((imgsize, imgsize))
        plt.imshow(tmp, cmap='Greys_r')

    return fig


def plot_loss(losses):
    pass


# source : https://stackoverflow.com/a/34325723
# Print iterations progress
def printProgressBar (iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()


if __name__ == '__main__':
    pass


def join_imgs(c_dim, images, size):
    imgs = (images + 1.) / 2.
    # imgs = images
    h, w = imgs.shape[1], imgs.shape[2]
    # join gambar
    img = np.zeros((int(h * size[0]), int(w * size[1]), c_dim))
    for idx, image in enumerate(imgs):
        i = idx % size[1]
        j = idx // size[1]
        img[j * h:j * h + h, i * w:i * w + w, :] = image
    img = np.reshape(img, (int(h * size[0]), int(w * size[1])))
    return img


def save_images2(self, path, images, size, c_dim):
    '''
    simpan output G sbg 1 gambar
    :param path: filename
    :param images: output dari generator
    :param size: ceil sqrt batch size (x, x)
    :return:
    '''
    img = join_imgs(c_dim, images, size)
    scipy.misc.imsave(path, (255*img).astype(np.uint8))


def create_labels(distribution):
    classes = len(distribution)
    total = sum(distribution)
    labels = np.zeros((total, classes)).astype(np.float32)
    v = []
    for i in range(classes):
        for j in range(distribution[i]):
            v.append(i)
    for i in range(total):
        labels[i][v[i]] = 1.
    return labels, v


if __name__ == '__main__':
    pass
