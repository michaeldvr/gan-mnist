import tensorflow as tf
from tensorflow.contrib import layers as tfl
from tensorflow.examples.tutorials.mnist import input_data

# ops
def dense(input, out_dim, name=None):
    return tf.layers.dense(input, out_dim, activation=None, name=name, use_bias=True)


def lrelu(input, alpha=0.01, name=None):
    return tf.nn.leaky_relu(input, alpha=alpha, name=name)

def relu(input, name=None, **kwargs):
    return tf.nn.relu(input, name=name)


class BatchNorm(object):
    def __init__(self, epsilon=1e-5, momentum=0.8, name='batch_norm'):
        with tf.variable_scope(name):
            self.epsilon = epsilon
            self.momentum = momentum
            self.name = name

    def __call__(self, x, is_training):
        return tfl.batch_norm(x, decay=self.momentum, updates_collections=None, epsilon=self.epsilon,
                              scale=True, is_training=is_training, scope=self.name)
    '''
    def __call__(self, x, is_training):
        return x
    '''


'''unused'''
def batch_norm2(input, is_training, decay=0.9):
    return tfl.batch_norm(input, decay=decay, is_training=is_training, scale=True, updates_collections=None)
    # return input


def batch_norm(input, is_training, momentum=0.99, epsilon=1e-3, name=None):
    return tf.layers.batch_normalization(input, momentum=momentum, epsilon=epsilon, training=is_training, name=name)


def dense_batch_lrelu(input, out_dim, scope, alpha=0.01, momentum=0.9, epsilon=1e-5, is_training=True):
    with tf.variable_scope(scope):
        h1 = dense(input, out_dim, name='dense')
        h2 = tfl.batch_norm(h1, momentum=momentum, epsilon=epsilon, training=is_training, name='bn')
        return lrelu(h2, alpha=alpha, name='lrelu')


def linear(input, out_dim, name='dense', stddev=0.02):
    stddev = tf.sqrt(2. / input.get_shape().as_list()[1])
    with tf.variable_scope(name):
        W = tf.get_variable('W', [input.get_shape()[1], out_dim], tf.float32,
                            tf.random_normal_initializer(stddev=stddev))
        '''W = tf.get_variable('W', [input.get_shape().as_list()[1], out_dim], tf.float32,
                            tfl.xavier_initializer(uniform=False))'''
        b = tf.get_variable('bias', [out_dim], initializer=tf.zeros_initializer())
        return tf.matmul(input, W) + b


def xavier_init(size):
    xavier_stddev = 1. / tf.sqrt(size[0] / 2.)
    return tf.random_normal(shape=size, stddev=xavier_stddev)


def xavier_stddev(input_dim):
    return 1. / tf.sqrt(input_dim / 2.)



'''
SAME padding output shape:
out_w : ceil(input.width / stride_w)
out_h : ceil(input.height / stride_h)
out_d : n_filter
'''
# [Note] conv2d filter shape [height, width, in_channels, out_channels]
def conv2d(input, n_filter, filter_w=5, filter_h=5, stride_w=2, stride_h=2, stddev=0.02, name='conv2d'):
    with tf.variable_scope(name):
        W = tf.get_variable('W', [filter_h, filter_w, input.get_shape()[-1], n_filter],
                            initializer=tf.truncated_normal_initializer(stddev=stddev))
        b = tf.get_variable('b', [n_filter], initializer=tf.constant_initializer(0.0))
        conv = tf.nn.bias_add(
            tf.nn.conv2d(input, W, strides=[1, stride_h, stride_w, 1], padding='SAME'),
            b)
        return conv

# [Note] conv2d_transpose filter shape [height, width, out_channels, in_channels]
def _conv2d_transpose(
        input, output_shape,
        filter_w=5, filter_h=5, stride_w=2, stride_h=2, stddev=0.02, name='conv2d_tr'):
    with tf.variable_scope(name):
        W = tf.get_variable('W', [filter_h, filter_w, output_shape[-1], input.get_shape()[-1]],
                            initializer=tf.random_normal_initializer(stddev=stddev))
        b = tf.get_variable('b', [output_shape[-1]], initializer=tf.constant_initializer(0.0))
        convtr = tf.nn.bias_add(
            tf.nn.conv2d_transpose(input, W,
                                   output_shape=output_shape,
                                   strides=[1, stride_h, stride_w, 1]),
            b
        )
        return convtr, W, b


def conv2d_transpose_old(
        input, output_shape,
        filter_w=5, filter_h=5, stride_w=2, stride_h=2, padding='SAME', stddev=0.02, name='conv2d_tr'):
    return upconvolution(input, output_shape[-1], filter_h, filter_w,
                         stride_h, stride_w, tf.random_normal_initializer(stddev=stddev),
                         tf.constant_initializer(0.0), name, padding='SAME')\
        , None, None

def conv2d_transpose(
        input, output_shape,
        filter_w=5, filter_h=5, stride_w=2, stride_h=2, padding='SAME', stddev=0.02, name='conv2d_tr'):
    return tf.layers.conv2d_transpose(input, output_shape[-1], (filter_h, filter_w),
                                      strides=(stride_h, stride_w), use_bias=True,
                                      kernel_initializer=tf.random_normal_initializer(stddev=stddev),
                                      padding=padding, name=name)


def upconvolution(input, output_channel_size, filter_size_h, filter_size_w,
                  stride_h, stride_w, init_w, init_b, layer_name,
                  dtype=tf.float32, data_format="NHWC", padding='VALID'):
    with tf.variable_scope(layer_name):
        # calculation of the output_shape:
        if data_format == "NHWC":
            input_channel_size = input.get_shape().as_list()[3]
            input_size_h = input.get_shape().as_list()[1]
            input_size_w = input.get_shape().as_list()[2]
            stride_shape = [1, stride_h, stride_w, 1]
            if padding == 'VALID':
                output_size_h = (input_size_h - 1) * stride_h + filter_size_h
                output_size_w = (input_size_w - 1) * stride_w + filter_size_w
            elif padding == 'SAME':
                output_size_h = (input_size_h - 1) * stride_h + 1
                output_size_w = (input_size_w - 1) * stride_w + 1
            else:
                raise ValueError("unknown padding")
            output_shape = tf.stack([tf.shape(input)[0],
                                     output_size_h, output_size_w,
                                     output_channel_size])
        elif data_format == "NCHW":
            input_channel_size = input.get_shape().as_list()[1]
            input_size_h = input.get_shape().as_list()[2]
            input_size_w = input.get_shape().as_list()[3]
            stride_shape = [1, 1, stride_h, stride_w]
            if padding == 'VALID':
                output_size_h = (input_size_h - 1) * stride_h + filter_size_h
                output_size_w = (input_size_w - 1) * stride_w + filter_size_w
            elif padding == 'SAME':
                output_size_h = (input_size_h - 1) * stride_h + 1
                output_size_w = (input_size_w - 1) * stride_w + 1
            else:
                raise ValueError("unknown padding")
            output_shape = tf.stack([tf.shape(input)[0],
                                     output_channel_size,
                                     output_size_h, output_size_w])
        else:
            raise ValueError("unknown data_format")

        # creating weights:
        shape = [filter_size_h, filter_size_w,
                 output_channel_size, input_channel_size]
        W_upconv = tf.get_variable("w", shape=shape, dtype=dtype,
                                   initializer=init_w)

        shape = [output_channel_size]
        b_upconv = tf.get_variable("b", shape=shape, dtype=dtype,
                                   initializer=init_b)

        upconv = tf.nn.conv2d_transpose(input, W_upconv, output_shape, stride_shape,
                                        padding=padding,
                                        data_format=data_format)
        output = tf.nn.bias_add(upconv, b_upconv, data_format=data_format)

        # Now output.get_shape() is equal (?,?,?,?) which can become a problem in the
        # next layers. This can be repaired by reshaping the tensor to its shape:
        output = tf.reshape(output, output_shape)
        # now the shape is back to (?, H, W, C) or (?, C, H, W)

        return output


# concat function
concat = tf.concat


def concat_label(x, y):
    x_shapes = x.get_shape() if isinstance(x, tf.Tensor) else x.shape
    y_shapes = y.get_shape() if isinstance(y, tf.Tensor) else y.shape
    return tf.concat([
        x, y*tf.ones([x_shapes[0], x_shapes[1], x_shapes[2], y_shapes[3]])], 3)