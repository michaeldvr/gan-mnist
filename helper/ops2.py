import tensorflow as tf


def linear(input, out_dim, name=None, stddev=0.02):
    stddev = tf.sqrt(2. / input.get_shape()[1])
    with tf.variable_scope(name):
        W = tf.get_variable('W', [input.get_shape()[1], out_dim], tf.float32,
                            tf.random_normal_initializer(stddev=stddev))
        b = tf.get_variable('bias', [out_dim], initializer=tf.zeros_initializer)
        return tf.matmul(input, W) + b


def xavier_stddev(input_dim):
    return 1. / tf.sqrt(input_dim / 2.)


def conv2d(input, n_kernel, k_h=5, k_w=5, s_h=2, s_w=2, stddev=0.02, name=None, padding='same'):
    with tf.variable_scope(name):
        return tf.layers.conv2d(input, n_kernel, (k_h, k_w), strides=(s_h, s_w),
                                use_bias=True, bias_initializer=tf.zeros_initializer(0.0),
                                kernel_initializer=tf.random_normal_initializer(stddev=stddev),
                                padding=padding)


def conv2d_transpose(input, n_kernel, k_h=5, k_w=5, s_h=2, s_w=2,
                     padding='valid', stddev=0.02, name=None):
    with tf.variable_scope(name):
        return tf.layers.conv2d_transpose(
            input, n_kernel, (k_h, k_w), (s_h, s_w), padding=padding, use_bias=True,
            bias_initializer=tf.zeros_initializer(0.0),
            kernel_initializer=tf.random_normal_initializer(stddev=stddev)
        )

def relu(input, name=None):
    return tf.nn.relu(input, name=name)


def lrelu(input, alpha=0.01, name=None):
    return tf.nn.leaky_relu(input, alpha=alpha, name=name)


def batchnorm(x, training, eps=1e-5, momentum=0.8):
    return tf.layers.batch_normalization(
        x, training=training, momentum=momentum, epsilon=eps)
