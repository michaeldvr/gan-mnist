import pickle
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
import argparse


def plot(
        file='./output/linear/single/losses.pkl',
        sampling=.05,
        window_length=51,
        polyorder=3):

    print('plotting', file)
    print('sampling {}'.format(sampling), '| window_length', window_length, '| polyorder', polyorder)

    with open(file, 'rb') as f:
        losses = pickle.load(f)

    fix, ax = plt.subplots()

    step = int(1. / sampling)
    x = range(1, len(losses) + 1, step)

    print('sampled', len(x), 'of', len(losses))

    losses = np.array(losses[::step])

    d_loss = savgol_filter(losses.T[0], window_length, polyorder)
    g_loss = savgol_filter(losses.T[1], window_length, polyorder)

    plt.plot(x, losses.T[0], color='#0000FF', label='Discriminator')
    plt.plot(x, losses.T[1], color='#FF6700', label='Generator')
    plt.plot(x, d_loss, color='#00FFFF')
    plt.plot(x, g_loss, color='#FFA500')
    plt.title("Training Losses")
    plt.grid(True)
    plt.xlabel('iteration')
    plt.ylabel('loss')
    lower = np.floor(min(losses.T[0].min(), losses.T[1].min()))
    upper = np.ceil(max(losses.T[0].max(), losses.T[1].max()))
    plt.yticks(np.arange(lower, upper, 1))
    plt.legend()
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file', type=str)
    parser.add_argument('-s', metavar='sampling', default=.05, type=float)
    parser.add_argument('-w', metavar='window_length', default=51, type=int)
    parser.add_argument('-p', metavar='polyorder', default=3, type=int)
    args = parser.parse_args()
    plot(args.file, sampling=args.s, window_length=args.w, polyorder=args.p)
