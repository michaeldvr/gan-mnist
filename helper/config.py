# unused
import pickle

keys = ['g_hidden', 'd_hidden', 'z_dim', 'c_dim', 'batch_size', 'epochs',
        'sample_size', 'image_size', 'alpha', 'learning_rate', 'momentum',
        'save_interval', 'sample_interval', 'image_shape', 'save_dir',
        'seed', 'model_name']


class Config(object):

    def __init__(self,
                 g_hidden=128, d_hidden=128,
                 z_dim=100, c_dim=1, batch_size=64, epochs=20, sample_size=16,
                 image_size=64, alpha=0.02, learning_rate=0.001,
                 momentum=0.9, save_interval=500, sample_interval=500,
                 seed=None, model_name='DCGAN',
                 save_dir='./output'):
        self.g_hidden = g_hidden
        self.d_hidden = d_hidden
        self.z_dim = z_dim
        self.c_dim = c_dim
        self.batch_size = batch_size
        self.epochs = epochs
        self.sample_size = sample_size
        self.image_size = image_size
        self.alpha = alpha
        self.learning_rate = learning_rate
        self.image_size = image_size
        self.momentum = momentum
        self.save_interval = save_interval
        self.sample_interval = sample_interval
        self.image_shape = [self.image_size, self.image_size, self.c_dim]
        self.save_dir = save_dir
        self.seed = seed
        self.model_name = model_name

    def __getattr__(self, item):
        return self.data.get(item, None)

    def save(self, target_file):
        with open(target_file, 'wb') as f:
            data = dict()
            for k in keys:
                data[k] = self.__getattr__(k)
            pickle.dump(data, f)

    def load(self, source_file):
        with open(source_file, 'rb') as f:
            data = pickle.load(f)
            for key in data.keys():
                self.__setattr__(key, data[key])
