import random as rnd
import numpy as np


def dataset_xo(size=4, ch=False, use_noise=True, max_zero_noise=0.2):
    def zero():
        return rnd.uniform(0.0, max_zero_noise) if use_noise else 0.0

    def one():
        return rnd.uniform(1 - max_zero_noise, 1.0) if use_noise else 1.0
    label = None  # (0, 1) = x, (1, 0) = o
    canvas = np.zeros((size, size))
    if rnd.random() <= 0.5:  # x
        for i in range(size):
            for j in range(size):
                if i == j or i + j == size - 1:
                    canvas[i, j] = one()
                else:
                    canvas[i, j] = zero()
        label = (0, 1)
    else:  # o
        for i in range(size):
            for j in range(size):
                if (i == 0 or i == size - 1) and (j > 0 and j < size - 1):
                    canvas[i, j] = one()
                elif (j == 0 or j == size - 1) and (i > 0 and i < size - 1):
                    canvas[i, j] = one()
                else:
                    canvas[i, j] = zero()
        label = (1, 0)
    result = np.reshape(canvas, (size, size, 1)) if ch else canvas
    return result, label


class DatasetXO(object):

    def __init__(
            self, n=10000, pad=None, seed=None, scale=False,
            flatten=False, sz=4, use_noise=True, max_zero_noise=0.2):
        '''
        load MNIST data
        :param datadir: save directory
        :param one_hot: one hot vector label
        :param pad: zero pad on images -> ((0,0), (a,b), (c,d), (0,0))
        :param seed: seed for shuffle
        :param scale: scale to [-1, 1]
        :param flatten: flat data to vector
        '''
        rnd.seed(seed)
        np.random.seed(seed)
        tmp = [dataset_xo(size=sz, ch=True, use_noise=use_noise, max_zero_noise=max_zero_noise)
               for i in range(n)]
        self.images = np.reshape([k[0] for k in tmp], (-1, sz, sz, 1))
        self.labels = np.array([k[1] for k in tmp])
        if pad is not None:
            self.images = np.pad(self.images, pad, mode='constant')
        self.seed = seed
        if scale:
            self.images = self.images * 2. - 1.
        if seed is not None:
            np.random.seed(seed)
        if flatten:
            self.images = self.images.reshape([-1] + [np.prod(self.images.shape[1:])])
        # private
        self.num_examples = self.images.shape[0]
        self._cur_idx = 0
        self._epochs = 0

    def next_batch(self, batch_size, shuffle=True, with_label=True):
        start = self._cur_idx
        # shuffle first epoch
        res_img, res_lbl = None, None
        if self._epochs == 0 and start == 0 and shuffle:
            indices = np.arange(self.num_examples)
            np.random.shuffle(indices)
            self.images = self.images[indices]
            self.labels = self.labels[indices]
        if start + batch_size > self.num_examples:
            self._epochs += 1
            remainder = self.num_examples - start
            images_0 = self.images[start:self.num_examples]
            labels_0 = self.labels[start:self.num_examples]
            if shuffle:
                indices = np.arange(self.num_examples)
                np.random.shuffle(indices)
                self.images = self.images[indices]
                self.labels = self.labels[indices]
            start = 0
            self._cur_idx = batch_size - remainder
            end = self._cur_idx
            images_1 = self.images[start:end]
            labels_1 = self.labels[start:end]
            res_img, res_lbl = np.concatenate((images_0, images_1), axis=0), np.concatenate((labels_0, labels_1), axis=0)
        else:
            self._cur_idx += batch_size
            end = self._cur_idx
            res_img, res_lbl = self.images[start:end], self.labels[start:end]
        if with_label:
            return res_img, res_lbl
        else:
            return res_img


def test():
    data = DatasetXO()
    epochs = 20
    batch_size = 64
    step = data.num_examples // batch_size
    maxctr = int(epochs * (data.num_examples // batch_size))
    print('epochs', epochs, '| batch size', batch_size, '| step', step, '|maxctr', maxctr)
    for epoch in range(1, epochs+1):
        for ctr in range(1, step + 1):
            print('ctr', (ctr+1), '; epoch', epoch)
            _, _ = data.next_batch(batch_size)


if __name__ == '__main__':
    test()
