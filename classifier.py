from helper import ops
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import os
import pickle
import argparse
import time
from shutil import copy2


def conv2d(input, n_filter, filter_w=5, filter_h=5, stride_w=1, stride_h=1, stddev=0.1, name='conv2d'):
    with tf.variable_scope(name):
        W = tf.get_variable('W', [filter_h, filter_w, input.get_shape()[-1], n_filter],
                            initializer=tf.truncated_normal_initializer(stddev=stddev))
        b = tf.get_variable('b', [n_filter], initializer=tf.constant_initializer(0.1))
        conv = tf.nn.bias_add(
            tf.nn.conv2d(input, W, strides=[1, stride_h, stride_w, 1], padding='SAME'),
            b)
        return conv


def max_pool(input, name=None):
    return tf.nn.max_pool(input, ksize=[1, 2, 2, 1],
                          strides=[1, 2, 2, 1], padding='SAME', name=name)


def linear(input, out_dim, name='dense', stddev=0.1):
    stddev = tf.sqrt(2. / input.get_shape().as_list()[1])
    with tf.variable_scope(name):
        W = tf.get_variable('W', [input.get_shape()[1], out_dim], tf.float32,
                            tf.random_normal_initializer(stddev=stddev))
        b = tf.get_variable('bias', [out_dim], initializer=tf.constant_initializer(0.1))
        return tf.matmul(input, W) + b


class ConvNet(object):
    def __init__(self, sess, checkdir='./output', model_name='ConvNet'):
        self.sess = sess
        self.checkdir = checkdir
        if not os.path.exists(self.checkdir):
            os.makedirs(self.checkdir)
            os.makedirs(os.path.join(self.checkdir, 'logs'))
        self.model_name = model_name
        self.build_model()

    def build_model(self):
        self.x = tf.placeholder(tf.float32, shape=[None, 784])
        self.y_ = tf.placeholder(tf.float32, shape=[None, 10])
        self.keep_prob = tf.placeholder(tf.float32)

        x_image = tf.reshape(self.x, [-1, 28, 28, 1])

        h_conv1 = ops.relu(conv2d(x_image, 32, name='h_conv1'))
        h_pool1 = max_pool(h_conv1)

        h_conv2 = ops.relu(conv2d(h_pool1, 64, name='h_conv2'))
        h_pool2 = max_pool(h_conv2)

        flatten = tf.layers.flatten(h_pool2)
        dense = linear(flatten, 1024, name='h_fc1')

        dropout = tf.nn.dropout(dense, self.keep_prob)
        self.y_conv = linear(dropout, 10, name='out')
        self.saver = tf.train.Saver(max_to_keep=1)

    def train(self, dataset):
        cross_entropy = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(labels=self.y_, logits=self.y_conv))
        loss_sum = tf.summary.scalar('loss', cross_entropy)
        train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
        correct_prediction = tf.equal(tf.argmax(self.y_conv, 1), tf.argmax(self.y_, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        accuracy_sum = tf.summary.scalar('accuracy', accuracy)
        self.sess.run(tf.global_variables_initializer())

        self.writer = tf.summary.FileWriter(os.path.join(self.checkdir, 'logs'), self.sess.graph)
        start_time = time.time()
        max_iter = 10000
        for i in range(1, max_iter):
            batch = dataset.train.next_batch(50)
            if i % 100 == 0:
                v_batch = dataset.validation.next_batch(50)  # validation
                val_accuracy = accuracy.eval(feed_dict={
                    self.x: v_batch[0], self.y_: v_batch[1], self.keep_prob: 1.0})
                print('step %d, validation accuracy %g' % (i, val_accuracy))
            self.sess.run(train_step, feed_dict={self.x: batch[0], self.y_: batch[1], self.keep_prob: 0.5})
            accuracy_str, loss_str = self.sess.run([accuracy_sum, loss_sum], feed_dict={
                self.x: batch[0], self.y_: batch[1], self.keep_prob: 1.0})
            self.writer.add_summary(accuracy_str, i)
            self.writer.add_summary(loss_str, i)
            if i % 100 == 1 or i == max_iter:
                self.save(i, self.checkdir)

        test_batch_size = 250
        step = int(dataset.test.num_examples / test_batch_size)
        ctr = 0.0
        for i in range(step):
            test_batch = dataset.test.next_batch(test_batch_size, shuffle=False)
            ctr += self.sess.run(accuracy, feed_dict={
                self.x: test_batch[0], self.y_: test_batch[1], self.keep_prob: 1.0})
        test_acc = ctr / step
        print('test accuracy %g' % test_acc)
        elapsed = time.time() - start_time
        elapsed = 'Training time {:2d}h {:2d}m {:7.4f}s\n'.\
            format(int(elapsed // 3600), int((elapsed % 3600) // 60), elapsed % 60)
        print(elapsed)
        with open(os.path.join(self.checkdir, 'accuracy.txt'), 'w') as f:
            f.write('test accuracy ' + str(test_acc))
            f.write('\n' + elapsed)

    def save(self, ctr, checkpoint_dir):
        if not os.path.exists(checkpoint_dir):
            os.makedirs(checkpoint_dir)
        self.saver.save(self.sess, os.path.join(checkpoint_dir, self.model_name), global_step=ctr)

    def restore(self, checkpoint_dir):
        print('Restore model from', checkpoint_dir)
        ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
        res = False
        if ckpt and ckpt.model_checkpoint_path:
            self.saver.restore(self.sess, ckpt.model_checkpoint_path)
            res = True
        return res

    def test(self, images, labels):
        correct_prediction = tf.equal(tf.argmax(self.y_conv, 1), tf.argmax(self.y_, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        train_accuracy = self.sess.run(accuracy, feed_dict={
            self.x: images, self.y_: labels, self.keep_prob: 1.0})
        return train_accuracy


def training(output_dir):
    data = input_data.read_data_sets('MNIST_data/', one_hot=True, validation_size=5000)
    print('[dataset]: training', data.train.num_examples, '| validation',
          data.validation.num_examples, '| test', data.test.num_examples)
    with tf.Session() as sess:
        model = ConvNet(sess, output_dir)
        fn = 'classifier.py'
        copy2(os.path.join('./', fn), os.path.join(model.checkdir, fn))
        model.train(data)


def classify_acc(data_file, checkpoint_dir):
    with open(data_file, 'rb') as f:
        data = pickle.load(f)
    imgs = data['images'].reshape((-1, 784))
    labels = data['labels']
    with tf.Session() as sess:
        model = ConvNet(sess)
        model.sess.run(tf.global_variables_initializer())
        model.restore(checkpoint_dir)
    return imgs, labels, model
    # time.sleep(5)  # tunggu 5 detik
    # print(model.test(imgs, labels))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    sbp = parser.add_subparsers(title='action')
    train_parser = sbp.add_parser('train')
    train_parser.set_defaults(action='train')
    train_parser.add_argument('outdir', type=str)
    test_parser = sbp.add_parser('test')
    test_parser.set_defaults(action='test')
    test_parser.add_argument('data_file', type=str)
    test_parser.add_argument('checkpoint_dir', type=str)
    args = parser.parse_args()
    if args.action == 'train':
        training(args.outdir)
    elif args.action == 'test':
        print('pakai console')

