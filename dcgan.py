import numpy as np
import os
import time
from helper.ops import *
from helper import utils
import pickle
import matplotlib.pyplot as plt
from helper.dataloader import MNIST

SOURCE_FILES = ['dcgan.py', './helper/ops.py', './helper/utils.py', './helper/dataloader.py']


class DeepConvGAN(object):

    def __init__(self, session, image_size=28, z_size=100, c_dim=1,
                 batch_size=64, epochs=100,
                 g_hidden=64, d_hidden=64,
                 alpha=0.01, momentum=0.99, epsilon=1e-3,
                 save_interval=500, sample_interval=250, smooth=0.1,
                 learning_rate=0.001, beta1=0.9,
                 model_name='GAN', save_dir='./output', description='', seed=None,
                 hp_file=None):
        self.sess = session
        self.image_size = image_size
        self.learning_rate = learning_rate
        self.beta1 = beta1
        self.z_size = z_size
        self.c_dim = c_dim
        self.batch_size = batch_size
        self.epochs = epochs
        self.image_shape = [image_size, image_size, c_dim]
        self.g_hidden = g_hidden
        self.d_hidden = d_hidden
        self.alpha = alpha
        self.momentum = momentum
        self.epsilon = epsilon
        self.model_name = model_name
        self.save_interval = save_interval
        self.sample_interval = sample_interval
        self.save_dir = save_dir
        self.start_time = 0
        self.losses = (0, 0)  # d, g
        self.counter = 0
        self.smooth = smooth
        self.description = description
        self.seed = seed

        if self.seed is not None:
            tf.set_random_seed(self.seed)
            np.random.seed(seed)

        # batch norm class
        self.d_bn, self.g_bn = [], []  # discriminator : h1, h2 | generator: h0, h1, h2
        for b in range(1, 3):
            self.d_bn.append(BatchNorm(name='d_bn{}'.format(b), epsilon=self.epsilon, momentum=self.momentum))
        for b in range(3):
            self.g_bn.append(BatchNorm(name='g_bn{}'.format(b), epsilon=self.epsilon, momentum=self.momentum))
        if hp_file is not None:
            self._load_hyperparam(hp_file)
        else:
            self._checkdir(self.save_dir)
            self._save_hyperparam()
        self.build_model()
        self.losses = []  # D, G

    def build_model(self):
        self.is_training = tf.placeholder(tf.bool, name='is_training')
        self.input_real = tf.placeholder(tf.float32, [None] + self.image_shape, name='input_real')
        self.input_z = tf.placeholder(tf.float32, [None, self.z_size], name='z')
        # self.input_z_sum = tf.summary.histogram('z', self.input_z)

        self.G = self.generator(self.input_z, training=self.is_training)
        self.D, self.d_logits = self.discriminator(self.input_real, training=self.is_training)  # discriminator gbr dataset (real)
        self.D_, self.d_logits_ = self.discriminator(self.G, reuse=True, training=self.is_training)  # discriminator gbr G (fake)

        # self.d_sum = tf.summary.histogram('d', self.D)
        # self.d__sum = tf.summary.histogram('d_', self.D_)
        self.g_sum = tf.summary.image('G', self.G)

        self.d_loss_real = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(logits=self.d_logits, labels=tf.ones_like(self.d_logits)))
        self.d_loss_fake = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(logits=self.d_logits_, labels=tf.zeros_like(self.d_logits_)))

        self.g_loss = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(logits=self.d_logits_, labels=tf.ones_like(self.d_logits_)))

        self.d_loss_real_sum = tf.summary.scalar('d_loss_real', self.d_loss_real)
        self.d_loss_fake_sum = tf.summary.scalar('d_loss_fake', self.d_loss_fake)

        self.d_loss = self.d_loss_real + self.d_loss_fake

        self.g_loss_sum = tf.summary.scalar('g_loss', self.g_loss)
        self.d_loss_sum = tf.summary.scalar('d_loss', self.d_loss)

        t_vars = tf.trainable_variables()
        self.g_vars = [var for var in t_vars if var.name.startswith('generator')]
        self.d_vars = [var for var in t_vars if var.name.startswith('discriminator')]

        self.saver = tf.train.Saver(max_to_keep=1)

    def train(self, dataset):
        self._train_summary()
        # update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        # with tf.control_dependencies(update_ops):
        # with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
        d_train_opt = tf.train.AdamOptimizer(self.learning_rate, beta1=self.beta1).\
            minimize(self.d_loss, var_list=self.d_vars)
        g_train_opt = tf.train.AdamOptimizer(self.learning_rate, beta1=self.beta1).\
            minimize(self.g_loss, var_list=self.g_vars)

        self.sess.run(tf.global_variables_initializer())

        '''self.g_all_sum = tf.summary.merge(
            [self.input_z_sum, self.d__sum, self.g_sum, self.d_loss_fake_sum, self.g_loss_sum])
        self.d_all_sum = tf.summary.merge(
            [self.input_z_sum, self.d_sum, self.d_loss_real_sum, self.d_loss_sum])'''

        self.g_all_sum = tf.summary.merge(
            [self.g_sum, self.d_loss_fake_sum, self.g_loss_sum])
        self.d_all_sum = tf.summary.merge(
            [self.d_loss_real_sum, self.d_loss_sum])

        self.writer = tf.summary.FileWriter(os.path.join(self.save_dir, 'logs'), self.sess.graph)

        maxctr = int(self.epochs * (dataset.num_examples // self.batch_size))
        self.start_time = time.time()
        print()
        print(time.strftime('=== [ start: %d %b %Y %H:%M:%S ] ===', time.localtime()))

        # baca model sebelumnya di sini

        start_epoch = 1
        sample_img_size = int(np.ceil(np.sqrt(self.batch_size)))
        step = dataset.num_examples // self.batch_size

        for epoch in range(start_epoch, self.epochs + 1):

            for idx in range(1, step + 1):
                batch_images = dataset.next_batch(self.batch_size)[0]
                batch_images = batch_images * 2. - 1.
                batch_z = np.random.uniform(-1., 1., [self.batch_size, self.z_size])
                # print('### batch_images shape', batch_images.shape)
                # train D
                _, summary_str = self.sess.run([d_train_opt, self.d_all_sum], feed_dict={
                    self.input_real: batch_images, self.input_z: batch_z, self.is_training: True})
                self.writer.add_summary(summary_str, self.counter)
                # train G
                _, summary_str = self.sess.run([g_train_opt, self.g_all_sum], feed_dict={
                    self.input_z: batch_z, self.is_training: True})
                self.writer.add_summary(summary_str, self.counter)

                # train G lagi
                _, summary_str = self.sess.run([g_train_opt, self.g_all_sum], feed_dict={
                    self.input_z: batch_z, self.is_training: True})
                self.writer.add_summary(summary_str, self.counter)

                self.counter += 1

                # print
                loss_d_real = self.d_loss_real.eval({self.input_real: batch_images, self.is_training: False})
                loss_d_fake = self.d_loss_fake.eval({self.input_z: batch_z, self.is_training: False})
                loss_g = self.g_loss.eval({self.input_z: batch_z, self.is_training: False})
                self.losses.append((loss_d_real + loss_d_fake, loss_g))
                '''print("Epoch [{:2d}] [{:4d}/{:4d}] | d_loss: {:.8f}, g_loss: {:.8f}".format(
                    epoch, idx, step, loss_d_fake+loss_d_real, loss_g))'''
                utils.printProgressBar(self.counter, maxctr,
                                       prefix='Epoch [{:2d}] [{:4d}/{:4d}] | d_loss: {:.8f}, g_loss: {:.8f}'.
                                       format(epoch, idx, step, loss_d_fake+loss_d_real, loss_g),
                                       length=20, decimals=2, fill='#')
                # simpan contoh gambar
                if np.mod(self.counter, self.sample_interval) == 1 or self.counter == maxctr:
                    sample_z = np.random.uniform(-1., 1., [16, self.z_size])
                    samples = self.sess.run(self.G, feed_dict={self.input_z: sample_z, self.is_training: False})
                    fn = os.path.join(self.save_dir, 'samples/train_{:02d}_{:04d}_{:06d}.png'.format(epoch, idx, self.counter))
                    samples = (samples + 1.) / 2.
                    # print(len(samples))
                    # utils.save_images2(fn, samples, (sample_img_size, sample_img_size), self.c_dim)
                    utils.save_images(fn, samples, self.image_size, boxes=(4, 4))
                # save checkpoint
                if np.mod(self.counter, self.save_interval) == 1 or self.counter == maxctr:
                    self.save(self.counter, os.path.join(self.save_dir, 'checkpoints'))

        finish_time = time.time() - self.start_time
        self.summary(finish_time)
        print('Training finished in {:2d}h {:2d}m {:7.4f}s'.
              format(int(finish_time // 3600), int((finish_time % 3600) // 60), finish_time % 60))

    '''
    def generator(self, z):
        with tf.variable_scope('generator', reuse=False):
            h0 = tf.nn.leaky_relu(tf.layers.dense(z, self.g_hidden, activation=None), alpha=self.alpha, name='g_h0_in')
            logits = tf.layers.dense(h0, np.prod(self.image_shape), activation=None)
            out = tf.reshape(tf.tanh(logits), [-1, self.image_size, self.image_size, self.c_dim], name="g_h1_out")
            return out

    def discriminator(self, images, reuse=False):
        with tf.variable_scope('discriminator', reuse=reuse):
            imgs = tf.layers.flatten(images, name='d_input')
            h0 = tf.nn.leaky_relu(
                tf.layers.dense(imgs, self.d_hidden, activation=None), alpha=self.alpha, name='d_h0_in')
            logits = tf.layers.dense(h0, 1, activation=None)
            out = tf.sigmoid(logits, name='d_h1_out')
            return out, logits
    '''

    '''def generator(self, z, training):
    with tf.variable_scope('generator', reuse=False):
        z_ = linear(z, self.g_hidden*4*4*8)  # 64 x 4 x 4 x 4 = 4096
        h0 = tf.reshape(z_, [-1, 4, 4, self.g_hidden*8])  # [1, 4, 4, 256]
        h0 = relu(self.g_bn[0](h0, training), name='h0')

        h1, _, _ = conv2d_transpose(h0, [self.batch_size, 8, 8, self.g_hidden*4], name='convt_h1')  # [64, 8, 8, 256]
        h1 = relu(self.g_bn[1](h1, training), name='h1')

        h2, _, _ = conv2d_transpose(h1, [self.batch_size, 16, 16, self.g_hidden*2], name='convt_h2')  # [64, 16, 16, 128]
        h2 = relu(self.g_bn[2](h2, training), name='h2')

        # h3, _, _ = conv2d_transpose(h2, [self.batch_size, 28, 28, self.g_hidden], name='h3')
        # h3 = relu(batch_norm(h3, training))

        h3, _, _ = conv2d_transpose(h2, [self.batch_size, 32, 32, self.c_dim], name='h3')  # [64, 32, 32, 1]
        return tf.tanh(h3, name='out')'''

    def generator(self, z, training):
        with tf.variable_scope('generator'):
            # self.z_ = linear(z, 784*self.g_hidden/8)  # 28*28*g_h/8 -> [?, 7, 7, g_h*2]
            self.z_ = linear(z, 7*7*self.g_hidden*4)  # [?, 7x7x256]
            h0 = tf.reshape(self.z_, [-1, 7, 7, self.g_hidden*4])  # [?, 7, 7, 256]
            h0 = conv2d(h0, self.g_hidden*2, stride_w=1, stride_h=1)  # [?, 7, 7, 128]
            h0 = relu(self.g_bn[0](h0, training), name='h0')

            h1 = conv2d_transpose(h0, [self.batch_size, 14, 14, self.g_hidden], name='convt_h1')  # [?, 14 14, 64]
            h1 = relu(self.g_bn[1](h1, training), name='h1')

            h2 = conv2d_transpose(h1, [self.batch_size, 28, 28, 1], name='convt_out')  # [?, 28, 28, 1]
            return tf.nn.tanh(h2, name='out')

    def discriminator(self, image, training, reuse=False):
        # image [?, 28, 28, 1]
        with tf.variable_scope('discriminator', reuse=reuse):
            h0 = lrelu(conv2d(image, self.d_hidden), alpha=self.alpha, name='h0')  # ?, 14, 14, 64

            h1 = conv2d(h0, self.d_hidden*2, name='conv_h1')  # ?, 7, 7, 128
            h1 = lrelu(
                self.d_bn[0](h1, training),
                alpha=self.alpha, name='h1')

            h2 = conv2d(h1, self.d_hidden * 4, name='conv_h2', stride_h=1, stride_w=1)  # ?, 7, 7, 256
            h2 = lrelu(
                self.d_bn[1](h2, training),
                alpha=self.alpha, name='h2')

            h3 = tf.reshape(h2, [-1, 7*7*4*self.d_hidden])  # [?, 7*7*256]
            h3 = linear(h3, 1, name='logit')

            out = tf.sigmoid(h3, name='d_sigmoid')  # [?, 1]

            return out, h3

    def save(self, counter, checkpoint_dir):
        if not os.path.exists(checkpoint_dir):
            os.makedirs(checkpoint_dir)
        self.saver.save(self.sess, os.path.join(checkpoint_dir, self.model_name), global_step=counter)

    def sample(self, sample_size=10):
        z = np.random.uniform(-1., 1., [sample_size, self.z_size])
        samples = self.sess.run(self.G, feed_dict={self.input_z: z, self.is_training: False})
        sz = int(np.ceil(np.sqrt(sample_size)))
        samples = utils.join_imgs(self.c_dim, samples, (sz, sz))
        plt.imshow(samples, cmap='Greys_r')
        return samples

    def restore(self, checkpoint_dir=None):
        if checkpoint_dir is None:
            checkpoint_dir = os.path.join(self.save_dir, 'checkpoints')
        print('Restore model from', checkpoint_dir)
        ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
        res = False
        if ckpt and ckpt.model_checkpoint_path:
            self.saver.restore(self.sess, ckpt.model_checkpoint_path)
            res = True
        return res

    def _checkdir(self, outdir):
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        if not os.path.exists(os.path.join(outdir, 'checkpoints')):
            os.makedirs(os.path.join(outdir, 'checkpoints'))
        if not os.path.exists(os.path.join(outdir, 'logs')):
            os.makedirs(os.path.join(outdir, 'logs'))
        if not os.path.exists(os.path.join(outdir, 'samples')):
            os.makedirs(os.path.join(outdir, 'samples'))
        with open(os.path.join(outdir, 'summary.txt'), 'w') as f:
            desc = vars(self)
            res = ['[' + self.model_name + ']\n']
            for key in desc.keys():
                if key not in ['start_time', 'losses', 'sess', 'counter']:
                    res.append(key + '\t' + str(desc[key]) + '\n')
            f.writelines(res)

    def _train_summary(self):
        with open(os.path.join(self.save_dir, 'summary.txt'), 'w') as f:
            desc = vars(self)
            res = ['[' + self.model_name + ']\n']
            for key in desc.keys():
                if key not in ['start_time', 'losses', 'sess', 'counter']:
                    res.append(key + '\t' + str(desc[key]) + '\n')
            f.writelines(res)

    def summary(self, finishtime):
        with open(os.path.join(self.save_dir, 'summary.txt'), 'a') as f:
            f.write('\n')
            f.write('Training time {:2d}h {:2d}m {:7.4f}s\n'.
                    format(int(finishtime // 3600), int((finishtime % 3600) // 60), finishtime % 60))
            epoch = self.counter / (dataset.num_examples // self.batch_size)
            f.write('Counter {:4d} , epoch {:5.2f}\n'.format(self.counter, epoch))
            if len(self.losses) > 0:
                f.write('d_loss: {:.8f}, g_loss: {:.8f}\n'.format(self.losses[-1][0], self.losses[-1][1]))
            else:
                f.write('- no training data -\n')
        self.losses = np.array(self.losses)
        with open(os.path.join(self.save_dir, 'losses.pkl'), 'wb') as f:
            pickle.dump(self.losses, f)

    def _load_hyperparam(self, file):
        import json
        data = {}
        with open(file, 'r') as f:
            data = json.load(f)
        for key in data.keys():
            self.__setattr__(key, data[key])

    def _save_hyperparam(self):
        import json
        vars = ['image_size', 'z_size', 'c_dim',
                'batch_size', 'epochs',
                'g_hidden', 'd_hidden', 'alpha',
                'save_interval', 'sample_interval', 'smooth',
                'learning_rate', 'beta1',
                'model_name', 'save_dir', 'description']
        data = {}
        for key in vars:
            data[key] = self.__getattribute__(key)
        with open(os.path.join(self.save_dir, 'config.pkl'), 'w') as f:
            json.dump(data, f)


if __name__ == '__main__':
    with tf.Session() as sess:
        bs, iter = 64, 200000
        # ep = int(iter * bs / 55000) + 1
        ep = 20
        iter = int(ep * 70000 / bs)
        seed = 2018
        print('total epochs:', ep, '| iteration:', iter, '| batch size:', bs)
        gan = DeepConvGAN(sess, epochs=ep, batch_size=bs, alpha=0.2, image_size=28,
                          save_interval=1000, sample_interval=250,
                          g_hidden=64, d_hidden=64,
                          momentum=0.9, epsilon=1e-5,
                          learning_rate=0.0002, beta1=0.5,
                          save_dir='./output/conv/180528-2321-4layer', model_name='DCGAN',
                          seed=seed,
                          description='dcgan 4 layer (1 single, 2 conv)')
        from shutil import copy2
        for i in SOURCE_FILES:
            copy2(os.path.join('./', i), os.path.join(gan.save_dir, i.split('/')[-1]))
        try:
            dataset = MNIST(datadir='MNIST_data/', seed=seed)
            gan.train(dataset)
        except KeyboardInterrupt:
            finish_time = time.time() - gan.start_time
            print()
            print('Training stopped after {:2d}h {:2d}m {:7.4f}s'.
                  format(int(finish_time // 3600), int((finish_time % 3600) // 60), finish_time % 60))
            gan.summary(finish_time)
