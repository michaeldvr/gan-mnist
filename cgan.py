import tensorflow as tf
import numpy as np
import os
from helper.dataloader import MNIST
import scipy.misc
import time
from helper.ops import *
from helper import utils
import pickle
import matplotlib.pyplot as plt

G_ACTIVATION = tf.sigmoid
SOURCE_FILES = ['cgan.py', './helper/ops.py', './helper/utils.py', './helper/dataloader.py']


class CGAN(object):
    def __init__(self, session, image_size=28, z_size=100, c_dim=1,
                 batch_size=64, epochs=100, y_dim=10,
                 g_hidden=(256,), d_hidden=(128,), alpha=0.01,
                 save_interval=500, sample_interval=250, smooth=0.1,
                 learning_rate=0.001, beta1=0.9,
                 model_name='GAN', save_dir='./output', description='',
                 seed=None,
                 hp_file=None):
        self.sess = session
        self.image_size = image_size
        self.learning_rate = learning_rate
        self.beta1 = beta1
        self.z_size = z_size
        self.c_dim = c_dim
        self.y_dim = y_dim
        self.batch_size = batch_size
        self.epochs = epochs
        self.image_shape = [image_size, image_size, c_dim]
        self.g_hidden = g_hidden
        self.d_hidden = d_hidden
        self.alpha = alpha
        self.model_name = model_name
        self.save_interval = save_interval
        self.sample_interval = sample_interval
        self.save_dir = save_dir
        self.start_time = 0
        self.losses = []  # [d, g]
        self.counter = 0
        self.smooth = smooth
        self.description = description
        self.seed = seed
        if self.seed is not None:
            tf.set_random_seed(self.seed)
            np.random.seed(seed)
        if hp_file is not None:
            self._load_hyperparam(hp_file)
        else:
            self._checkdir(self.save_dir)
            self._save_hyperparam()
        self.build_model()


    def build_model(self):
        self.input_real = tf.placeholder(tf.float32, [None] + self.image_shape, name='input_real')
        self.input_z = tf.placeholder(tf.float32, [None, self.z_size], name='z')
        self.y = tf.placeholder(tf.float32, [None, self.y_dim], name='y')

        self.G = self.generator(self.input_z, self.y)
        self.D, self.d_logits = self.discriminator(self.input_real, self.y)  # discriminator dataset
        self.D_, self.d_logits_ = self.discriminator(self.G, self.y, reuse=True)  # discriminator generator

        self.g_sum = tf.summary.image('G', self.G)

        self.d_loss_real = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(logits=self.d_logits, labels=tf.ones_like(self.d_logits)))
        self.d_loss_fake = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(logits=self.d_logits_, labels=tf.zeros_like(self.d_logits_)))

        self.g_loss = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(logits=self.d_logits_, labels=tf.ones_like(self.d_logits_)))

        self.d_loss_real_sum = tf.summary.scalar('d_loss_real', self.d_loss_real)
        self.d_loss_fake_sum = tf.summary.scalar('d_loss_fake', self.d_loss_fake)

        self.d_loss = self.d_loss_real + self.d_loss_fake

        self.g_loss_sum = tf.summary.scalar('g_loss', self.g_loss)
        self.d_loss_sum = tf.summary.scalar('d_loss', self.d_loss)

        t_vars = tf.trainable_variables()
        self.g_vars = [var for var in t_vars if var.name.startswith('generator')]
        self.d_vars = [var for var in t_vars if var.name.startswith('discriminator')]

        self.saver = tf.train.Saver(max_to_keep=1)

    def train(self, dataset):
        self._train_summary()
        d_train_opt = tf.train.AdamOptimizer(self.learning_rate, beta1=self.beta1).\
            minimize(self.d_loss, var_list=self.d_vars)
        g_train_opt = tf.train.AdamOptimizer(self.learning_rate, beta1=self.beta1).\
            minimize(self.g_loss, var_list=self.g_vars)
        self.sess.run(tf.global_variables_initializer())

        self.g_all_sum = tf.summary.merge(
            [self.g_sum, self.d_loss_fake_sum, self.g_loss_sum])
        self.d_all_sum = tf.summary.merge(
            [self.d_loss_real_sum, self.d_loss_sum])

        self.writer = tf.summary.FileWriter(os.path.join(self.save_dir, 'logs'), self.sess.graph)

        maxctr = int(self.epochs * (dataset.num_examples // self.batch_size))
        self.start_time = time.time()
        print()
        print(time.strftime('=== [ start: %d %b %Y %H:%M:%S ] ===', time.localtime()))

        # sampling config
        n_sample = 16
        boxes = int(np.ceil(np.sqrt(n_sample)))
        boxes = (boxes, boxes)

        start_epochs = 1
        step = dataset.num_examples // self.batch_size
        for epoch in range(start_epochs, self.epochs + 1):
            for idx in range(1, step + 1):
                batch_images, batch_labels = dataset.next_batch(self.batch_size)
                batch_z = np.random.uniform(-1., 1., [self.batch_size, self.z_size])

                _, summary_str = self.sess.run([d_train_opt, self.d_all_sum], feed_dict={
                    self.input_real: batch_images, self.input_z: batch_z, self.y: batch_labels})
                self.writer.add_summary(summary_str, self.counter)
                _, summary_str = self.sess.run([g_train_opt, self.g_all_sum], feed_dict={
                    self.input_z: batch_z, self.y: batch_labels})
                self.writer.add_summary(summary_str, self.counter)

                self.counter += 1

                loss_d_real = self.d_loss_real.eval({self.input_real: batch_images, self.y: batch_labels})
                loss_d_fake = self.d_loss_fake.eval({self.input_z: batch_z, self.y: batch_labels})
                loss_g = self.g_loss.eval({self.input_z: batch_z, self.y: batch_labels})
                self.losses.append((loss_d_real + loss_d_fake, loss_g))
                utils.printProgressBar(self.counter, maxctr,
                                       prefix='Epoch [{:2d}] [{:4d}/{:4d}] | d_loss: {:.8f}, g_loss: {:.8f}'.
                                       format(epoch, idx, step, loss_d_fake + loss_d_real, loss_g),
                                       length=20, decimals=2, fill='#')
                # simpan contoh gambar
                if np.mod(self.counter, self.sample_interval) == 1 or self.counter == maxctr:

                    sample_z = np.random.uniform(-1., 1., [n_sample, self.z_size])
                    sample_y = np.zeros((n_sample, self.y_dim)).astype(np.float32)
                    for i in range(n_sample):
                        sample_y[i][i % self.y_dim] = 1.
                    samples = self.sess.run(self.G, feed_dict={self.input_z: sample_z, self.y: sample_y})
                    fn = os.path.join(self.save_dir,
                                      'samples/train_{:02d}_{:04d}_{:06d}.png'.format(epoch, idx, self.counter))
                    samples = (samples + 1.) / 2.
                    # print(len(samples))
                    # utils.save_images2(fn, samples, (sample_img_size, sample_img_size), self.c_dim)
                    utils.save_images(fn, samples, self.image_size, boxes=boxes)
                # save checkpoint
                if np.mod(self.counter, self.save_interval) == 1 or self.counter == maxctr:
                    self.save(self.counter, os.path.join(self.save_dir, 'checkpoints'))

        finish_time = time.time() - self.start_time
        self.summary(finish_time)
        print('Training finished in {:2d}h {:2d}m {:7.4f}s'.
              format(int(finish_time // 3600), int((finish_time % 3600) // 60), finish_time % 60))

    def generator(self, z, y):
        with tf.variable_scope('generator'):

            # h_z = relu(linear(z, 200, name='g_h_z'))
            # h_y = relu(linear(y, 1000, name='g_h_y'))

            z_ = tf.concat([z, y], axis=1)
            h = []
            for i in range(len(self.g_hidden)):
                h.append(
                    relu(linear(h[-1] if i > 0 else z_, self.g_hidden[i], name='g_h{}'.format(i))))
            logits = linear(h[-1], np.prod(self.image_shape), name='g_logits')
            out = tf.reshape(G_ACTIVATION(
                logits), [-1, self.image_size, self.image_size, self.c_dim],
                name='g_out')
            return out

    def discriminator(self, images, y, reuse=False):
        with tf.variable_scope('discriminator', reuse=reuse):
            imgs = tf.layers.flatten(images, name='d_input_img')  # [?, 784]

            # h_imgs = relu(linear(imgs, 256), name='d_h_imgs')
            # h_y = relu(linear(y, 128), name='d_h_y')

            x = tf.concat([imgs, y], axis=1, name='d_input')  # [?, 794]
            h = []
            for i in range(len(self.d_hidden)):
                h.append(
                    relu(linear(h[-1] if i > 0 else x, self.d_hidden[i], name='d_h{}'.format(i))))
            logits = linear(h[-1], 1, name='d_logits')
            out = tf.sigmoid(logits, name='d_out')
            return out, logits

    def save(self, counter, checkpoint_dir):
        if not os.path.exists(checkpoint_dir):
            os.makedirs(checkpoint_dir)
        self.saver.save(self.sess, os.path.join(checkpoint_dir, self.model_name), global_step=counter)

    def sample(self, labels, join_imgs=False):
        sample_size = len(labels)
        z = np.random.uniform(-1., 1., [sample_size, self.z_size])
        samples = self.sess.run(self.G, feed_dict={self.input_z: z, self.y: labels})
        if join_imgs:
            sz = int(np.ceil(np.sqrt(sample_size)))
            samples = self._join_imgs(samples, (sz, sz))
        # samples = samples.reshape((self.image_size, self.image_size))
        # plt.imshow(samples, cmap='Greys_r')
        return samples

    def restore(self, checkpoint_dir=None):
        if checkpoint_dir is None:
            checkpoint_dir = os.path.join(self.save_dir, 'checkpoints')
        print('Restore model from', checkpoint_dir)
        ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
        res = False
        if ckpt and ckpt.model_checkpoint_path:
            self.saver.restore(self.sess, ckpt.model_checkpoint_path)
            res = True
        return res

    def _save_images(self, path, images, size):
        '''
        simpan output G sbg 1 gambar
        :param path: filename
        :param images: output dari generator
        :param size: ceil sqrt batch size (x, x)
        :return:
        '''
        img = self._join_imgs(images, size)
        scipy.misc.imsave(path, (255*img).astype(np.uint8))

    def _join_imgs(self, images, size):
        imgs = (images + 1.) / 2.
        # imgs = images
        h, w = imgs.shape[1], imgs.shape[2]
        # join gambar
        img = np.zeros((int(h * size[0]), int(w * size[1]), self.c_dim))
        for idx, image in enumerate(imgs):
            i = idx % size[1]
            j = idx // size[1]
            img[j * h:j * h + h, i * w:i * w + w, :] = image
        img = np.reshape(img, (int(h * size[0]), int(w * size[1])))
        return img

    def _checkdir(self, outdir):
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        if not os.path.exists(os.path.join(outdir, 'checkpoints')):
            os.makedirs(os.path.join(outdir, 'checkpoints'))
        if not os.path.exists(os.path.join(outdir, 'logs')):
            os.makedirs(os.path.join(outdir, 'logs'))
        if not os.path.exists(os.path.join(outdir, 'samples')):
            os.makedirs(os.path.join(outdir, 'samples'))
        with open(os.path.join(outdir, 'summary.txt'), 'w') as f:
            desc = vars(self)
            res = ['[' + self.model_name + ']\n']
            for key in desc.keys():
                if key not in ['start_time', 'losses', 'sess', 'counter']:
                    res.append(key + '\t' + str(desc[key]) + '\n')
            f.writelines(res)

    def _train_summary(self):
        with open(os.path.join(self.save_dir, 'summary.txt'), 'w') as f:
            desc = vars(self)
            res = ['[' + self.model_name + ']\n']
            for key in desc.keys():
                if key not in ['start_time', 'losses', 'sess', 'counter']:
                    res.append(key + '\t' + str(desc[key]) + '\n')
            f.writelines(res)

    def summary(self, finishtime, num_data=70000):
        with open(os.path.join(self.save_dir, 'summary.txt'), 'a') as f:
            f.write('\n')
            f.write('Training time {:2d}h {:2d}m {:7.4f}s\n'.
                    format(int(finishtime // 3600), int((finishtime % 3600) // 60), finishtime % 60))
            epoch = self.counter / (num_data // self.batch_size)
            f.write('Counter {:4d} , epoch {:5.2f}\n'.format(self.counter, epoch))
            if len(self.losses) > 0:
                f.write('d_loss: {:.8f}, g_loss: {:.8f}\n'.format(self.losses[-1][0], self.losses[-1][1]))
            else:
                f.write('- no training data -\n')
        self.losses = np.array(self.losses)
        with open(os.path.join(self.save_dir, 'losses.pkl'), 'wb') as f:
            pickle.dump(self.losses, f)

    def _load_hyperparam(self, file):
        import json
        data = {}
        with open(file, 'r') as f:
            data = json.load(f)
        for key in data.keys():
            self.__setattr__(key, data[key])

    def _save_hyperparam(self):
        import json
        vars = ['image_size', 'z_size', 'c_dim', 'y_dim',
                'batch_size', 'epochs',
                'g_hidden', 'd_hidden', 'alpha',
                'save_interval', 'sample_interval', 'smooth',
                'learning_rate', 'beta1',
                'model_name', 'save_dir', 'description']
        data = {}
        for key in vars:
            data[key] = self.__getattribute__(key)
        with open(os.path.join(self.save_dir, 'config.pkl'), 'w') as f:
            json.dump(data, f)


if __name__ == '__main__':
    with tf.Session() as sess:
        bs, iter = 128, 201000
        ep = int(iter * bs / 55000) + 1
        # ep = 20
        iter = int(ep * 70000 / bs)
        seed = None
        print('total epochs:', ep, '| iteration:', iter, '| batch size:', bs)
        gan = CGAN(sess, epochs=ep, batch_size=bs, alpha=0.1,
                   save_interval=1000, sample_interval=500,
                   g_hidden=(128,), d_hidden=(128,),
                   learning_rate=0.001, beta1=0.9, seed=seed,
                   save_dir='./output/cgan/180529-1751-linear-200K-iter', model_name='CGAN',
                   description='''
                   batch size 128, 200K iterasi
                   generator 2, discriminator 1 (hidden layer size 128),
                   generator sigmoid activation, ReLU, no scaling
                   ''')
        from shutil import copy2

        for i in SOURCE_FILES:
            copy2(os.path.join('./', i), os.path.join(gan.save_dir, i.split('/')[-1]))
        try:
            dataset = MNIST(datadir='MNIST_data/', seed=seed)
            gan.train(dataset)
        except KeyboardInterrupt:
            finish_time = time.time() - gan.start_time
            print()
            print('Training stopped after {:2d}h {:2d}m {:7.4f}s'.
                  format(int(finish_time // 3600), int((finish_time % 3600) // 60), finish_time % 60))
            gan.summary(finish_time)