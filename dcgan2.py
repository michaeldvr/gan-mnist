import numpy as np
from helper.ops2 import *
from helper.dataloader import MNIST
from helper import utils
from helper import config
import os
import matplotlib.pyplot as plt
import time
import pickle


class DCGAN2(object):
    def __init__(self, session, conf):
        self.sess = session
        self.conf = conf if conf is not None else config.Config()
        if self.conf.seed is not None:
            tf.random_seed = self.conf.seed
            np.random.seed(self.conf.seed)
        self.build_model()
        self._checkdir(self.conf.save_dir)

    def discriminator(self, x, training, reuse=False):
        with tf.variable_scope('discriminator', reuse=reuse):
            h0 = conv2d(x, self.conf.d_hidden, name='d_h0')
            h0 = lrelu(batchnorm(h0, training, momentum=self.conf.momentum),
                       self.conf.alpha)

            h1 = conv2d(h0, self.conf.d_hidden*2, name='d_h1')
            h1 = lrelu(batchnorm(h1, training, momentum=self.conf.momentum),
                       self.conf.alpha)

            h2 = tf.layers.flatten(h1)
            h2 = linear(h2, 1, 'd_linear', stddev=xavier_stddev(h2.get_shape()[-1]))

            return tf.sigmoid(h2, name='d_out'), h2

    def generator(self, z, training):
        with tf.variable_scope('generator'):
            h0 = linear(z, 7*7*self.conf.g_hidden*2, stddev=xavier_stddev(z.get_shape()[-1]))
            h0 = tf.reshape(h0, (-1, 7, 7, self.conf.g_hidden*2), name='g_h0')
            h0 = relu(batchnorm(h0, training, momentum=self.conf.momentum))

            h1 = conv2d_transpose(h0, self.conf.g_hidden, padding='same')  # 14, 14, g_hidden*2
            h1 = relu(batchnorm(h1, training, momentum=self.conf.momentum), name='g_h1')

            h2 = conv2d_transpose(h1, 1, padding='same')  # 32, 32, g_hidden
            h2 = relu(batchnorm(h2, training, momentum=self.conf.momentum), name='g_h2')

            return tf.nn.tanh(h2, name='g_out')

    def build_model(self):
        # placeholders
        self.training = tf.placeholder(tf.bool, name='training')
        self.input_real = tf.placeholder(tf.float32, [None] + self.conf.image_shape, name='input_real')
        self.input_z = tf.placeholder(tf.float32, [None, self.conf.z_dim], name='z')

        # networks
        self.G = self.generator(self.input_z, self.training)
        self.D, self.d_logits = self.discriminator(self.input_real, self.training)  # real / data discriminator
        self.D_, self.d_logits_ = self.discriminator(self.G, self.training, reuse=True)

        # loss
        self.d_loss_real = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(logits=self.d_logits, labels=tf.ones_like(self.d_logits)))
        self.d_loss_fake = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(logits=self.d_logits_, labels=tf.zeros_like(self.d_logits_)))
        self.d_loss = self.d_loss_real + self.d_loss_fake
        self.g_loss = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(logits=self.d_logits_, labels=tf.ones_like(self.d_logits_)))

        # summary
        self.g_sum = tf.summary.image('G', self.G)
        self.d_loss_real_sum = tf.summary.scalar('d_loss_real', self.d_loss_real)
        self.d_loss_fake_sum = tf.summary.scalar('d_loss_fake', self.d_loss_fake)
        self.d_loss_sum = tf.summary.scalar('d_loss', self.d_loss)
        self.g_loss_sum = tf.summary.scalar('g_loss', self.g_loss)

        t_vars = tf.trainable_variables()
        self.g_vars = [var for var in t_vars if var.name.startswith('generator')]
        self.d_vars = [var for var in t_vars if var.name.startswith('discriminator')]

        self.saver = tf.train.Saver(max_to_keep=1)

    def train(self, dataset):
        with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
            d_train_opt = tf.train.AdamOptimizer(self.conf.learning_rate)\
                .minimize(self.d_loss, var_list=self.d_vars)
            g_train_opt = tf.train.AdamOptimizer(self.conf.learning_rate)\
                .minimize(self.g_loss, var_list=self.g_vars)

        self.sess.run(tf.global_variables_initializer())

        self.g_all_sum = tf.summary.merge(
            [self.g_sum, self.d_loss_fake, self.g_loss_sum])
        self.d_all_sum = tf.summary.merge(
            [self.d_loss_real_sum, self.d_loss_sum])

        self.writer = tf.summary.FileWriter(os.path.join(self.conf.save_dir, 'logs'), self.sess.graph)

        maxctr = int(self.conf.epochs * (dataset.num_examples // self.conf.batch_size))
        self.start_time = time.time()
        print()
        print(time.strftime('=== [ start: %d %b %Y %H:%M:%S ] ===', time.localtime()))

        start_epoch = 1
        step = dataset.num_examples // self.conf.batch_size
        self.counter = 1
        self.losses = []  # D, G

        for epoch in range(start_epoch, self.conf.epochs + 1):
            for idx in range(1, step + 1):
                batch_images = dataset.next_batch(self.conf.batch_size)[0]
                batch_images = batch_images * 2. - 1.
                batch_z = np.random.uniform(-1., 1., [self.conf.batch_size, self.conf.z_dim])

                _, summary_str = self.sess.run([d_train_opt, self.d_all_sum], feed_dict={
                    self.input_real: batch_images, self.input_z: batch_z, self.training: True})
                self.writer.add_summary(summary_str, self.counter)

                _, summary_str = self.sess.run([g_train_opt, self.g_all_sum], feed_dict={
                    self.input_z: batch_z, self.training: True})
                self.writer.add_summary(summary_str, self.counter)

                self.counter += 1
                loss_d_real = self.d_loss_real.eval({self.input_real: batch_images, self.training: False})
                loss_d_fake = self.d_loss_fake.eval({self.input_z: batch_z, self.training: False})
                loss_g = self.g_loss.eval({self.input_z: batch_z, self.training: False})
                self.losses.append((loss_d_real + loss_d_fake, loss_g))

                utils.printProgressBar(self.counter, maxctr,
                                       prefix='Epoch [{:2d}] [{:4d}/{:4d}] | d_loss: {:.8f}, g_loss: {:.8f}'.
                                       format(epoch, idx, step, loss_d_fake + loss_d_real, loss_g),
                                       length=20, decimals=2, fill='#')

                # sampling
                if np.mod(self.counter, self.conf.sample_interval) == 1 or self.counter == maxctr:
                    sample_z = np.random.uniform(-1., 1., [self.conf.sample_size, self.conf.z_dim])
                    samples = self.sess.run(self.G, feed_dict={self.input_z: sample_z, self.training: False})
                    fn = os.path.join(self.conf.save_dir, 'samples/train_{:02d}_{:04d}_{:06d}.png'.format(epoch, idx, self.counter))
                    samples = (samples + 1.) / 2.
                    utils.save_images(fn, samples, self.conf.image_size, boxes=(4, 4))
                # checkpoint
                if np.mod(self.counter, self.conf.save_interval) == 1 or self.counter == maxctr:
                    self.save(self.counter, os.path.join(self.conf.save_dir, 'checkpoints'))

        # finished training
        finish_time = time.time() - self.start_time
        self.summary(finish_time)
        print('Training finished in {:2d}h {:2d}m {:7.4f}s'.
              format(int(finish_time // 3600), int((finish_time % 3600) // 60), finish_time % 60))


    def sample(self, sample_size=16, display=False):
        z = np.random.uniform(-1., 1., [sample_size, self.conf.z_dim])
        samples = self.sess.run(self.G, feed_dict={self.input_z: z, self.training: False})
        sz = int(np.ceil(np.sqrt(sample_size)))
        samples = utils.join_imgs(self.conf.c_dim, samples, (sz, sz))
        if display:
            plt.imshow(samples, cmap='Greys_r')
        return samples

    def save(self, counter, checkpoint_dir):
        if not os.path.exists(checkpoint_dir):
            os.makedirs(checkpoint_dir)
        self.saver.save(self.sess,
                        os.path.join(checkpoint_dir, self.conf.model_name),
                        global_step=counter)

    def restore(self, checkpoint_dir=None):
        pass

    def _checkdir(self, outdir):
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        if not os.path.exists(os.path.join(outdir, 'checkpoints')):
            os.makedirs(os.path.join(outdir, 'checkpoints'))
        if not os.path.exists(os.path.join(outdir, 'logs')):
            os.makedirs(os.path.join(outdir, 'logs'))
        if not os.path.exists(os.path.join(outdir, 'samples')):
            os.makedirs(os.path.join(outdir, 'samples'))
        with open(os.path.join(outdir, 'summary.txt'), 'w') as f:
            desc = config.keys
            res = ['[' + self.conf.model_name + ']\n']
            for key in desc:
                if key not in ['start_time', 'losses', 'sess', 'counter']:
                    res.append(key + '\t' + str(self.conf.__getattr__(key)) + '\n')
            f.writelines(res)

    def _write_summary(self, line):
        with open(os.path.join(self.conf.save_dir, 'summary.txt'), 'a') as f:
            f.write(line + '\n')

    def summary(self, finishtime):
        with open(os.path.join(self.conf.save_dir, 'summary.txt'), 'a') as f:
            f.write('\n')
            f.write('Training time {:2d}h {:2d}m {:7.4f}s\n'.
                    format(int(finishtime // 3600), int((finishtime % 3600) // 60), finishtime % 60))
            epoch = self.counter / (dataset.num_examples // self.conf.batch_size)
            f.write('Counter {:4d} , epoch {:5.2f}\n'.format(self.counter, epoch))
            if len(self.losses) > 0:
                f.write('d_loss: {:.8f}, g_loss: {:.8f}\n'.format(self.losses[-1][0], self.losses[-1][1]))
            else:
                f.write('- no training data -\n')
        self.losses = np.array(self.losses)
        with open(os.path.join(self.conf.save_dir, 'losses.pkl'), 'wb') as f:
            pickle.dump(self.losses, f)


if __name__ == '__main__':
    dataset = MNIST(datadir='MNIST_data/')
    batch_size, iteration = 64, 50000
    epochs = int(iteration * batch_size / dataset.num_examples) + 1
    # epochs = 20
    iteration = int(epochs * dataset.num_examples / batch_size)
    print('total epochs:', epochs, '| iteration:', iter, '| batch size:', batch_size)
    conf = config.Config(epochs=epochs, batch_size=batch_size, alpha=0.2, image_size=28,
                         save_interval=1000, sample_interval=500,
                         g_hidden=64, d_hidden=64, sample_size=16,
                         momentum=0.99, learning_rate=0.0002, model_name='DCGAN',
                         save_dir='./output/nconv/180528-0027')
    sess = tf.Session()
    gan = DCGAN2(sess, conf)
    try:
        gan.train(dataset)
    except KeyboardInterrupt:
        finish_time = time.time() - gan.start_time
        print()
        print('Training stopped after {:2d}h {:2d}m {:7.4f}s'.
              format(int(finish_time // 3600), int((finish_time % 3600) // 60), finish_time % 60))
        gan.summary(finish_time)
