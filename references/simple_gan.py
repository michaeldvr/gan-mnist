import tensorflow as tf
import numpy as np
import pickle as pkl
import matplotlib.pyplot as plt
from os.path import join

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('MNIST_data/')

OUT_DIR = './output'


def model_inputs(real_dim, z_dim):
    inputs_real = tf.placeholder(tf.float32, (None, real_dim), name='input_real')
    inputs_z = tf.placeholder(tf.float32, (None, z_dim), name='input_z')
    return inputs_real, inputs_z


def generator(z, out_dim, n_units=128, reuse=False, alpha=0.01):
    with tf.variable_scope('generator', reuse=reuse):
        h1 = tf.nn.leaky_relu(tf.layers.dense(z, n_units, activation=None), alpha=alpha)
        logits = tf.layers.dense(h1, out_dim, activation=None)
        out = tf.tanh(logits)
        return out


def discriminator(x, n_units=128, reuse=False, alpha=0.01):
    with tf.variable_scope('discriminator', reuse=reuse):
        h1 = tf.nn.leaky_relu(tf.layers.dense(x, n_units, activation=None), alpha=alpha)
        logits = tf.layers.dense(h1, 1, activation=None)
        out = tf.sigmoid(logits)
        return out, logits

# hyperparams
input_size = 784
z_size = 100
g_hidden_size = 128
d_hidden_size = 128
alpha = 0.01
smooth = 0.1

# build network
tf.reset_default_graph()
input_real, input_z = model_inputs(input_size, z_size)

z_sum = tf.summary.histogram('z', input_z)

g_model = generator(input_z, input_size, n_units=g_hidden_size, alpha=alpha)

# g_sum = tf.summary.histogram('G', g_model)

d_model_real, d_logits_real = discriminator(input_real, n_units=d_hidden_size, alpha=alpha)
d_model_fake, d_logits_fake = discriminator(g_model, reuse=True, n_units=d_hidden_size, alpha=alpha)

d_real_sum = tf.summary.histogram('d_real', d_model_real)
d_fake_sum = tf.summary.histogram('d_fake', d_model_fake)

# losses
d_loss_real = tf.reduce_mean(
    tf.nn.sigmoid_cross_entropy_with_logits(logits=d_logits_real, labels=tf.ones_like(d_logits_real) * (1 - smooth)))
d_loss_fake = tf.reduce_mean(
    tf.nn.sigmoid_cross_entropy_with_logits(logits=d_logits_fake, labels=tf.zeros_like(d_logits_real)))

d_loss_real_sum = tf.summary.histogram('d_loss_real', d_loss_real)
d_loss_fake_sum = tf.summary.histogram('d_loss_fake', d_loss_fake)

d_loss = d_loss_real + d_loss_fake

g_loss = tf.reduce_mean(
    tf.nn.sigmoid_cross_entropy_with_logits(logits=d_logits_fake, labels=tf.ones_like(d_logits_fake)))

d_loss_sum = tf.summary.histogram('d_loss', d_loss)
g_loss_sum = tf.summary.histogram('g_loss', g_loss)

# optimizers
learning_rate = 0.002
t_vars = tf.trainable_variables()
g_vars = [var for var in t_vars if var.name.startswith('generator')]
d_vars = [var for var in t_vars if var.name.startswith('discriminator')]

d_train_opt = tf.train.AdamOptimizer(learning_rate).minimize(d_loss, var_list=d_vars)
g_train_opt = tf.train.AdamOptimizer(learning_rate).minimize(g_loss, var_list=g_vars)

# summary
g_sum = tf.summary.merge([z_sum, d_fake_sum, d_loss_fake_sum, g_loss_sum])
d_sum = tf.summary.merge([z_sum, d_real_sum, d_loss_real_sum, d_loss_sum])

# training
batch_size = 100
epochs = 100
samples = []
losses = []
# save generator variables
saver = tf.train.Saver(max_to_keep=1)
batch_images = []
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    writer = tf.summary.FileWriter(join(OUT_DIR, 'logs'), sess.graph)
    ctr = 1
    for e in range(epochs):
        for ii in range(mnist.train.num_examples // batch_size):
            batch = mnist.train.next_batch(batch_size)

            batch_images = batch[0].reshape((batch_size, 784))
            batch_images = batch_images * 2 - 1

            batch_z = np.random.uniform(-1, 1, size=(batch_size, z_size))

            _, summary_str = sess.run([d_train_opt, d_sum], feed_dict={input_real: batch_images, input_z: batch_z})
            writer.add_summary(summary_str, ctr)
            _, summary_str = sess.run([g_train_opt, g_sum], feed_dict={input_z: batch_z, input_real: batch_images})
            writer.add_summary(summary_str, ctr)

            ctr += 1

        # print loss per epoch
        train_loss_d = sess.run(d_loss, {input_z: batch_z, input_real: batch_images})
        train_loss_g = g_loss.eval({input_z: batch_z})

        print("Epoch {}/{}...".format(e + 1, epochs),
              "Discriminator Loss: {:.4f}...".format(train_loss_d),
              "Generator Loss: {:.4f}".format(train_loss_g))
        # Save losses to view after training
        losses.append((train_loss_d, train_loss_g))

        # Sample from generator as we're training for viewing afterwards
        sample_z = np.random.uniform(-1, 1, size=(16, z_size))
        gen_samples = sess.run(
            generator(input_z, input_size, reuse=True),
            feed_dict={input_z: sample_z})
        samples.append(gen_samples)
        saver.save(sess, join(OUT_DIR, 'checkpoints/generator.ckpt'), global_step=ctr)

# Save training generator samples
with open(join(OUT_DIR, 'train_samples.pkl'), 'wb') as f:
    pkl.dump(samples, f)

fig, ax = plt.subplots()
losses = np.array(losses)
plt.plot(losses.T[0], label='Discriminator')
plt.plot(losses.T[1], label='Generator')
plt.title("Training Losses")
plt.legend()
plt.show()


if __name__ == '__main__':
    pass
