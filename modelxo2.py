from helper.xo_data import DatasetXO
import tensorflow as tf
import tensorflow.contrib as tfc
import numpy as np
from helper.ops import *
from helper.utils import printProgressBar


def generator(z, kernel_size=(3,3), kernel_stride=2, img_size=(4, 4), depth=2):
    h0 = linear(z, np.prod(img_size)//4, name='h0')
    h0_reshape = tf.reshape(h0, (-1, img_size[0], img_size[1], 1), name='h0_reshape')
    h1 = conv2d_transpose(h0_reshape, [depth], filter_h=kernel_size[0],
                          filter_w=kernel_size[1], stride_h=kernel_stride,
                          stride_w=kernel_stride, name='h1')
    h1_relu = relu(h1, name='h1_relu')
    h2 = conv2d(h1_relu, 1, filter_h=kernel_size[0], filter_w=kernel_size[1],
                stride_h=kernel_stride, stride_w=kernel_stride, name='h2')
    out = tf.nn.sigmoid(h2, name='out')
    opts = {
        'h0': h0,
        'h0_reshape': h0_reshape,
        'h1': h1,
        'h1_relu': h1_relu,
        'h2': h2,
        'out': out
    }
    return out, opts


def discriminator(img, kernel_size=(3, 3), kernel_stride=2, img_size=(4,4),
                  depth=2, alpha=0.1):
    h0 = conv2d(img, depth, filter_h=kernel_size[0], filter_w=kernel_size[1],
                stride_h=kernel_stride, stride_w=kernel_stride, name='h0')
    h0_act = tf.nn.leaky_relu(h0, alpha=alpha, name='h0_act')
    h1 = conv2d(img, depth*2, filter_h=kernel_size[0], filter_w=kernel_size[1],
                stride_h=kernel_stride, stride_w=kernel_stride, name='h1')
    h1_act = tf.nn.leaky_relu(h1, alpha=alpha, name='h1_act')
    h1_flatten = tfc.layers.flatten(h1_act)
    mbdisc = minibatch(h1_flatten)
    h2 = linear(mbdisc, 1, name='h2')
    out = tf.nn.sigmoid(h2, 'out')
    opts = {
        'h0': h0,
        'h0_act': h0_act,
        'h1': h1,
        'h1_act': h1_act,
        'h1_flatten': h1_flatten,
        'mbdisc': mbdisc,
        'h2': h2,
        'out': out
    }
    return out, h2, opts


def log(x):
    return tf.log(tf.maximum(x, 1e-5))


def optimizer(loss, var_list, learning_rate, beta1=0.9):
    train = tf.train.AdamOptimizer(learning_rate, beta1)
    optim = train.compute_gradients(loss, var_list)
    return train, optim


class GANXO(object):

    def __init__(self, sess,
                 z_dim=5, img_size=(4,4), learning_rate=0.01,
                 alpha=0.1, minibatch_size=16, kernel_depth=2,
                 beta1=0.9):
        self.sess = sess
        self.z_dim = z_dim
        self.img_size = img_size
        self.image_shape = [img_size, img_size, 1]
        self.learning_rate = learning_rate
        self.alpha = alpha
        self.minibatch_size = minibatch_size
        self.kernel_depth = kernel_depth
        self.beta1 = beta1

    def build_model(self):
        self.input_z = tf.placeholder(tf.float32, [self.minibatch_size, self.z_dim])
        self.input_real = tf.placeholder(tf.float32, [self.minibatch_size] + self.image_shape)
        # network
        with tf.variable_scope('generator'):
            self.G, self.G_opts = generator(
                self.input_z, img_size=(self.img_size, self.img_size),
                depth=self.kernel_depth
            )
        with tf.variable_scope('discriminator'):
            self.D, self.D_logits, self.D_opts = discriminator(
                self.input_real, img_size=(self.img_size, self.img_size),
                alpha=self.alpha, depth=self.kernel_depth
            )
        # fake discriminator
        with tf.variable_scope('discriminator', reuse=True):
            self.D_, self.D_logits_, self.D_opts_ = discriminator(
                self.G, img_size=(self.img_size, self.img_size),
                alpha=self.alpha, depth=self.kernel_depth
            )
        self.loss_d = tf.reduce_mean(-log(self.D) - log(1 - log(self.D_)))
        self.loss_g = tf.reduce_mean(-log(self.D_))
        self.var_list = tf.trainable_variables()
        self.g_vars = [v for v in self.var_list if v.name.startswith('generator')]
        self.d_vars = [v for v in self.var_list if v.name.startswith('discriminator')]

        self.g_train, self.g_optim = optimizer(self.loss_g, self.g_vars, self.learning_rate, self.beta1)
        self.d_train, self.d_optim = optimizer(self.loss_d, self.d_vars, self.learning_rate, self.beta1)

        self.sess.run(tf.global_variables_initializer())

    def train(self, dataset, epochs, sample_ctr=100, save_dir='./output/XO'):
        ep_iter = dataset.num_examples // self.minibatch_size
        for epoch in range(1, epochs+1):
            for i in range(1, ep_iter+1):
                z = np.random.random((self.minibatch_size, self.z_dim))
                data = dataset.next_batch(self.minibatch_size)[0].reshape()


if __name__ == '__main__':
    pass